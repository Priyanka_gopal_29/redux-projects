import React, { useState,useEffect } from "react";
import 'antd/dist/antd.css';
import { Card,Row,Col,Modal,Button,Input,Search } from 'antd';
import { connect } from "react-redux";
import { fetchUsers,searchData,showdata,filterDetails } from "../redux";


const UserContainer=({ userData, fetchUsers,searchData,showdata,filterDetails })=> {
 const { Search } = Input;
//  const [visible, setVisible]=useState(false);
//  const [search,setSearch]=useState('');
//  const [filteredPersons,setFilteredPersons] = useState([]);

 useEffect(() => {
    filterDetails(
      userData.users.filter((person) => {
        const firstName = person.name.first.toLowerCase();
        const lastName = person.name.last.toLowerCase();
        const fullName = firstName + " " + lastName;
        return fullName.includes(userData.search.toLowerCase());
        console.log(userData.search)
      })
    );
  }, [filterDetails,userData.search, userData.users]);


   const showModal =()=>{
        showdata(true)
        // setVisible(true);
    }
    const handleOk =(e)=>{
        showdata(false)
        // setVisible(false);
    }
    const handleCancel =(e)=>{
      showdata(false)
      // setVisible(false);
    }
     
  useEffect(() => {

    fetchUsers();

  }, [fetchUsers]);

  return userData.loading ? (
    <h2>Loading...</h2>
  ) : userData.error ? (
    <h2>{userData.error}</h2>
  ) : (
    <div style={{width:"100%"}}>
          <Search
                type="text"
                placeholder="input search text"
                onChange={e=>searchData(e.target.value)}
                style={{ width: 500,borderRadius:"15px",height:40,marginTop:30 }}
                />
              <Row>
                {userData.filters.map(( item,index) => 
                   <Col key={index} span={8}>
                        <Card style={{ width: "93%",height:180 ,background:"#F6F3F3",marginTop:30,marginLeft:20}}>
                            <Row>
                                <Col span={10}><img alt="Oval" src={item.picture.large}></img></Col>
                                <Col span={14}>
                                   <div key={index}>Title : {item.name.title}</div>
                                   <div key={index}>Name: {item.name.first}{item.name.last}</div>
                                   <div key={index}>Location: {item.location.street.number}{item.location.street.name}</div>
                                   <div key={index}>Country : {item.location.country}</div>
                                   <div key={index}>State : {item.location.state}</div>
                                   <div key={index}><Button onClick={showModal}> Details</Button></div>
                                </Col>
                            </Row>
                        </Card>
                        
                         <Modal 
                            title="User Details"
                            visible={userData.visible}
                            onOk={handleOk}
                            onCancel={handleCancel}
                          >
                           <Row>
                                <Col span={9}><img alt="Oval" src={item.picture.large} ></img></Col>
                                <Col span={15}>
                                   <div key={index}>Title : {item.name.title}</div>
                                   <div key={index}>Name: {item.name.first}{item.name.last}</div>
                                   <div key={index}>Location: {item.location.street.number}{item.location.street.name}</div>
                                </Col>  
                            </Row>
                        </Modal> 
                          
                    </Col>
                )}
            </Row>
            </div>
  );
}

const mapStateToProps = (state) => {
  return {
    userData: state.user
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUsers: () => dispatch(fetchUsers()),
    searchData: (search)=> dispatch(searchData(search)),
    showdata:(visible)=>dispatch(showdata(visible)),
    filterDetails:(filters)=>dispatch(filterDetails(filters))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserContainer);













