import {
  FETCH_USERS_REQUEST,
  FETCH_USERS_SUCCESS,
  FETCH_USERS_FAILURE,
  SEARCH,
  SHOW_MODAL,
  FILTER_DATA
} from "./userTypes";

const initialState = {
  loading: false,
  users: [],
  error: "",
  search:'',
  visible:false,
  filters:[]
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_USERS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case FETCH_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        users: action.payload,
        error: "",
      };
    case FETCH_USERS_FAILURE:
      return {
        ...state,
        loading: false,
        users: [],
        error: action.payload,
      };
      case SEARCH: {
        return {
            ...state,
            search: action.payload
        }
    }
    case SHOW_MODAL: {
        return {
            ...state,
            visible: action.payload
        }
    }
    case FILTER_DATA: {
        return {
            ...state,
            filters: action.payload
        }
    }
    default:
      return state;
  }
};

export default reducer;
